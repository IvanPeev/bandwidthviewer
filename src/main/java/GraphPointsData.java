import java.awt.*;
import java.util.ArrayList;

public class GraphPointsData {
	private ArrayList<Point> downloadPoints;
	private ArrayList<Point> uploadPoints;

	public void setDownloadPoints(ArrayList<Point> points) { this.downloadPoints = points; }
	public void setUploadPoints(ArrayList<Point> points) { this.uploadPoints = points; }

	public ArrayList<Point> getDownloadPoints() { return downloadPoints; }
	public ArrayList<Point> getUploadPoints() { return uploadPoints; }
}
