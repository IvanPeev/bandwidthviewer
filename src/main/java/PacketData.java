public class PacketData {
	private double uploadSize;
	private double downloadSize;
	private String label;
	private long epochSecond;
	private int sizeFactor;

	public void setUploadSize(double uploadSize) { this.uploadSize = uploadSize; }
	public void setDownloadSize(double downloadSize) { this.downloadSize = downloadSize; }
	public void setLabel(String label) { this.label = label; }
	public void setEpochSecond(long epochSecond) { this.epochSecond = epochSecond; }
	public void setSizeFactor(int sizeFactor) { this.sizeFactor = sizeFactor; }

	public double getUploadSize() { return uploadSize; }
	public double getDownloadSize() { return downloadSize; }
	public String getLabel() { return label; }
	public long getEpochSecond() { return epochSecond; }
	public int getSizeFactor() { return sizeFactor; }
}
