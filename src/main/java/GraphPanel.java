import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GraphPanel extends JPanel {
	private final int PADDING = 25;
	private final int LABEL_PADDING = 25;
	private final Color UPLOAD_COLOR = new Color(44, 102, 230, 180);
	private final Color DOWNLOAD_COLOR = new Color(200, 40, 30, 200);
	private final Stroke GRAPH_STROKE = new BasicStroke(2f);
	private ArrayList<PacketData> packets;

	public GraphPanel(ArrayList<PacketData> packets) {
		this.packets = packets;
//		for (var packet: packets) {
//			System.out.printf("| %20s | %20s | %15s | %15s |\n", packet.getDownloadSize(), packet.getUploadSize(), packet.getEpochSecond(), packet.getLabel());
//		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		GraphPointsData graphPointsData = createGraphPoints();
		this.drawBackground(g2);
		this.drawAxes(g2);
		this.drawLines(g2, graphPointsData);
	}

	public void setPackets(ArrayList<PacketData> packets) {
		this.packets = packets;
		invalidate();
		this.repaint();
	}

	private double getMinData() {
		double minData = Double.MAX_VALUE;
		for (var packet: packets) {
			double packetMin = Math.min(packet.getDownloadSize(), packet.getUploadSize());
			minData = Math.min(minData, packetMin);
		}
		return minData;
	}

	private double getMaxData() {
		double maxData = Double.MIN_VALUE;
		for (var packet: packets) {
			double packetMax = Math.max(packet.getDownloadSize(), packet.getUploadSize());
			maxData = Math.max(maxData, packetMax);
		}
		return maxData;
	}

	private GraphPointsData createGraphPoints() {
		double xScale = ((double) getWidth() - (2 * PADDING) - LABEL_PADDING) / (packets.size() - 1);
		double yScale = ((double) getHeight() - 2 * PADDING - LABEL_PADDING) / (getMaxData() - getMinData());
		var points = new GraphPointsData();
		var uploadPoints = new ArrayList<Point>();
		var downloadPoints = new ArrayList<Point>();

		for (int i = 0; i < packets.size(); i++) {
			int x = (int) (i * xScale + PADDING + LABEL_PADDING);
			int y1 = (int) (((getMaxData() - packets.get(i).getDownloadSize()) * yScale) + PADDING);
			int y2 = (int) (((getMaxData()) - packets.get(i).getUploadSize()) * yScale + PADDING);

			downloadPoints.add(new Point(x, y1));
			uploadPoints.add(new Point(x, y2));
		}

		points.setDownloadPoints(downloadPoints);
		points.setUploadPoints(uploadPoints);

		return points;
	}

	public void drawBackground(Graphics2D g) {
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(Color.BLACK);
	}

	public void drawAxes(Graphics2D g) {
		g.setColor(new Color(200, 200, 200, 200));
		// x axis
		g.drawLine(PADDING + LABEL_PADDING, getHeight() - PADDING - LABEL_PADDING, getWidth() - PADDING, getHeight() - PADDING - LABEL_PADDING);
		// y axis
		g.drawLine(PADDING + LABEL_PADDING, getHeight() - PADDING - LABEL_PADDING, PADDING + LABEL_PADDING, PADDING);
		g.drawString("s", getWidth() - PADDING + 10, getHeight() - PADDING - LABEL_PADDING + 3);
	}

	private void drawLines(Graphics2D g, GraphPointsData graphPointsData) {
		var uploadPoints = graphPointsData.getUploadPoints();
		var downloadPoints = graphPointsData.getDownloadPoints();

		g.setStroke(GRAPH_STROKE);
		g.setColor(UPLOAD_COLOR);

		// draw upload lines
		for (int i = 0; i < uploadPoints.size() - 1; i++) {
			int x1 = uploadPoints.get(i).x;
			int y1 = uploadPoints.get(i).y;
			int x2 = uploadPoints.get(i + 1).x;
			int y2 = uploadPoints.get(i + 1).y;
			g.drawLine(x1, y1, x2, y2);
		}

		g.setColor(DOWNLOAD_COLOR);

		// draw download lines
		for (int i = 0; i < downloadPoints.size() - 1; i++) {
			int x1 = downloadPoints.get(i).x;
			int y1 = downloadPoints.get(i).y;
			int x2 = downloadPoints.get(i + 1).x;
			int y2 = downloadPoints.get(i + 1).y;
			g.drawLine(x1, y1, x2, y2);
		}
	}
}