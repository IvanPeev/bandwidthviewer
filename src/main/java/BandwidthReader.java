import org.pcap4j.core.*;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.packet.IpV4Packet;

import javax.swing.*;
import java.awt.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.ArrayList;

public class BandwidthReader {
	private final String INTERFACE_IP_ADDRESS = "192.168.1.8";
	private ArrayList<PacketData> packetsData = new ArrayList<>();
	private GraphPanel graphPanel = null;

	public void initialize() throws UnknownHostException, PcapNativeException {
		InetAddress address = InetAddress.getByName(INTERFACE_IP_ADDRESS);
		PcapNetworkInterface networkInterface = Pcaps.getDevByAddress(address);

		int snapLen = 65536;
		int timeout = 1;
		PromiscuousMode mode = PromiscuousMode.PROMISCUOUS;
		PcapHandle handle = networkInterface.openLive(snapLen, mode, timeout);
		readPackets(handle, networkInterface);
	}

	private void readPackets(PcapHandle handle, PcapNetworkInterface networkInterface) {
		PacketListener listener = packet -> handlePacketReading(getPacket(packet));

		try {
			// loop infinitely
			int maxPackets = -1;
			handle.loop(maxPackets, listener);
		} catch (InterruptedException | PcapNativeException | NotOpenException e) {
			e.printStackTrace();
		}
	}

	private void handlePacketReading(IpV4Packet ipv4Packet) {
		insertPacketData(ipv4Packet);
		var summedPackets = sumEpochSecondsData(packetsData);
		var consistentPackets = createConsistentEpochPackets(summedPackets);
		paint(consistentPackets);
	}

	private void insertPacketData(IpV4Packet ipv4Packet) {
		if (ipv4Packet != null) {
			PacketData packet = new PacketData();
			packet.setEpochSecond(Instant.now().getEpochSecond());

			if (ipv4Packet.getHeader().getDstAddr().toString().equals('/' + INTERFACE_IP_ADDRESS)) {
				packet.setDownloadSize(ipv4Packet.getPayload().getRawData().length * 8);
				packet.setUploadSize(0);
			}
			if (ipv4Packet.getHeader().getSrcAddr().toString().equals('/' + INTERFACE_IP_ADDRESS)) {
				packet.setUploadSize(ipv4Packet.getPayload().getRawData().length * 8);
				packet.setDownloadSize(0);
			}

			packetsData.add(packet);
		}
	}

	private IpV4Packet getPacket(PcapPacket pcapPacket) {
		try {
			return pcapPacket.get(IpV4Packet.class);
		} catch (NullPointerException e) {
			return null;
		}
	}

	private ArrayList<PacketData> createConsistentEpochPackets(ArrayList<PacketData> summedPackets) {
		var consistentPackets = new ArrayList<PacketData>();
		int summedPacketsSize = summedPackets.size();

		for (int i = 0; i < summedPacketsSize; i++) {
			consistentPackets.add(summedPackets.get(i));

			if (i < summedPacketsSize - 1) {
				int epochDifference = (int) (summedPackets.get(i + 1).getEpochSecond() - summedPackets.get(i).getEpochSecond());

				if (epochDifference > 1) {
					for (int j = 1; j < epochDifference; j++) {
						PacketData emptyPacket = new PacketData();
						emptyPacket.setDownloadSize(0);
						emptyPacket.setUploadSize(0);
						emptyPacket.setEpochSecond(summedPackets.get(i).getEpochSecond() + j);
						emptyPacket.setSizeFactor(0);
						setCurrentSizeLabel(0, emptyPacket);
						consistentPackets.add(emptyPacket);
					}
				}
			}
		}

		return consistentPackets;
	}

	private void paint(ArrayList<PacketData> consistentPackets) {
		if (graphPanel == null) {
			graphPanel = new GraphPanel(consistentPackets);
			graphPanel.setPreferredSize(new Dimension(700, 500));

			var mainPanel = new JPanel();
			mainPanel.setPreferredSize(new Dimension(700, 500));
			mainPanel.add(graphPanel);
			mainPanel.setBackground(Color.darkGray);

			var labelPanel = new JPanel();
			var downloadLabel = new JLabel("download: ");
			var uploadLabel = new JLabel("upload: ");

			// TODO: add download/upload color labels
			// TODO: add max download/upload speed
			// TODO: calculate bandwidth correctly for yScale
			// TODO: add axes labels
			// TODO: paint bandwidth for with max 60s once 6s is reached, ex: 1-61s, 2-62s, 3-63s etc.

//			downloadLabel.setLocation(100, 20);
//			uploadLabel.setLocation(130, 20);

//			uploadLabel.setForeground(new Color(200, 200, 200, 200));
//
//			downloadLabel.setForeground(new Color(200, 200, 200, 200));
//			downloadLabel.setBackground(Color.RED);
//			downloadLabel.setPreferredSize(new Dimension(85, 450));
//			downloadLabel.setVerticalAlignment(SwingConstants.BOTTOM);

//			labelPanel.add(downloadLabel);
//			labelPanel.add(uploadLabel);

			labelPanel.setBackground(Color.BLACK);
			labelPanel.setPreferredSize(new Dimension(150, 500));

			var frame = new JFrame("Bandwidth Viewer");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.add(mainPanel);
			frame.add(labelPanel);

			frame.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
			frame.pack();
			frame.setBackground(Color.DARK_GRAY);
			frame.setResizable(false);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		} else {
			graphPanel.setPackets(consistentPackets);
		}
	}

	private ArrayList<PacketData> sumEpochSecondsData(ArrayList<PacketData> packets) {
		var sumPackets = new ArrayList<PacketData>();
		ArrayList<ArrayList<PacketData>> epochPacketsCollection = new ArrayList<>();
		long currentEpochSecond = packets.get(0).getEpochSecond();
		int i = 0;

		epochPacketsCollection.add(new ArrayList<>());

		for (PacketData packet: packets) {
			if (packet.getEpochSecond() != currentEpochSecond) {
				i++;
				epochPacketsCollection.add(new ArrayList<>());
				currentEpochSecond = packet.getEpochSecond();
			}
			epochPacketsCollection.get(i).add(packet);
		}

		for (ArrayList<PacketData> epochPackets: epochPacketsCollection) {
			double totalDownload = 0;
			double totalUpload = 0;

			for (PacketData epochPacket: epochPackets) {
				totalDownload += epochPacket.getDownloadSize();
				totalUpload += epochPacket.getUploadSize();
			}

			PacketData sumPacket = new PacketData();

			selectDataSize(sumPacket, totalDownload, totalUpload);
			sumPacket.setEpochSecond(epochPackets.get(0).getEpochSecond());
			sumPackets.add(sumPacket);
		}

		return sumPackets;
	}

	private void selectDataSize(PacketData packet, double totalDownload, double totalUpload) {
		double bits = Math.max(totalDownload, totalUpload);
		int sizeFactor = 0;

		if (bits >= 1024 && bits < Math.pow(1024, 2)) sizeFactor = 1;
		if (bits >= Math.pow(1024, 2) && bits < Math.pow(1024, 3)) sizeFactor = 2;
		if (bits >= Math.pow(1024, 3) && bits < Math.pow(1024, 4)) sizeFactor = 3;

		packet.setDownloadSize(totalDownload / Math.pow(1024, sizeFactor));
		packet.setUploadSize(totalUpload / Math.pow(1024, sizeFactor));
		packet.setSizeFactor(sizeFactor);
		setCurrentSizeLabel(sizeFactor, packet);
	}

	private void setCurrentSizeLabel(int size, PacketData packet) {
		if (size == 0) packet.setLabel("B");
		if (size == 1) packet.setLabel("KiB");
		if (size == 2) packet.setLabel("MiB");
		if (size == 3) packet.setLabel("GiB");
	}
}